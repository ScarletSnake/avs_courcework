FROM tonistiigi/debian:riscv

WORKDIR /tmp/litmus
RUN git clone https://github.com/litmus-tests/litmus-tests-riscv.git
RUN apt-get update && apt-get install -y make \
apt-get update && apt-get install bc \
apt-get update && apt-get install msort \
	
RUN /usr/bin/ ln -s msort msort7 \
apt-get install opam \
apt update && apt install gcc \
apt update && apt install build-essential && apt-get install manpages-dev

RUN opam init \
opam switch create 4.11.0 \
rm -rf .opam/ \
opam init --comp=4.05.0 --disable-sandboxing --reinit \
opam depext conf-m4 \
opam install herdtools7 \
opam update \
opam upgrade \

WORKDIR /tmp/litmus/litmus-tests-riscv-master
RUN make run-hw-tests CORES=4 \
tar -zxf hw-tests-src.tgz \
cd hw-tests-src \
make [GCC=<gcc>] [-j <m>] \
./run.sh
